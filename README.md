# [Interactive Cellular Automaton](https://cellular-stuff.netlify.app)

Educative and interactive one-dimensional binary cellular automaton generation web site.

You'll find the developing code on the **develop branch** and the code that will be displayed on the webpage in the **master branch**.

This project was generated with **Angular CLI** version **9.1.9**.

In order to run the code in a development server:
- Run _ng serve_ on the main folder. 
- Navigate to http://localhost:4200/ on any browser.

The app will automatically reload if you change any of the source files.
