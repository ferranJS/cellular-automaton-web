
export default class d1CelAutom{
  constructor(){}
  
  ruleList(num: number) : number[][] {
    var numB = num.toString(2);
    var times = 8 - numB.length;
    for(let i=0; i<times; i++) {
      numB = '0' + numB;
    }
    var rule = [
      [0,0,0, Number(numB[7])],
      [0,0,1, Number(numB[6])],
      [0,1,0, Number(numB[5])],
      [0,1,1, Number(numB[4])],
      [1,0,0, Number(numB[3])],
      [1,0,1, Number(numB[2])],
      [1,1,0, Number(numB[1])],
      [1,1,1, Number(numB[0])]
    ]
    return rule;
  }

  ruleListInv(list: string) : number {
    var num = parseInt( list.split('').reverse().join(''), 2 );
    return num;
  }
  
  nextConfig(ACini: number[], num: number[]) : number[] {
    //var rule = this.ruleList(num);
    var rule = num;
    var len = ACini.length;
    var res = [];
    var j = 0;
    for(j=0; j<8; j++){
      if(ACini[len-1] == rule[j][0] && 
        ACini[0] == rule[j][1] && 
        ACini[1] == rule[j][2]) {
          break;
      }
    }
    res[0] = rule[j][3];
  
    for(let i=1; i<len-1; i++) {
      for(j=0; j<8; j++){
        if(ACini[i-1] == rule[j][0] && 
          ACini[i] == rule[j][1] && 
          ACini[i+1] == rule[j][2]) {
            break;
        }
      }    
      res[i] = rule[j][3];
    }
  
    for(j=0; j<8; j++){
      if(ACini[len-2] == rule[j][0] && 
        ACini[len-1] == rule[j][1] && 
        ACini[0] == rule[j][2]) {
          break;
      }
    }
    res[len-1] = rule[j][3];
    return res;
  }
}